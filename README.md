# Plotting utility classes for Java

[![pipeline status](https://gitlab.com/de.monochromata/de.monochromata.plot/badges/master/pipeline.svg)](https://gitlab.com/de.monochromata/de.monochromata.plot/commits/master)

This library mainly wraps [JMathPlot](https://github.com/yannrichet/jmathplot).

* It is available from a Maven repository at https://monochromata-de.gitlab.io/de.monochromata.plot/m2/ .
* The Maven site is available at https://monochromata-de.gitlab.io/de.monochromata.plot/ .
* An Eclipse p2 repository is available at https://monochromata-de.gitlab.io/de.monochromata.plot/p2repo/ .

## Links

The project is used by
[de.monochromata.eyetracking](https://gitlab.com/monochromata-de/de.monochromata.eyetracking/) and
[de.monochromata.ml](https://gitlab.com/monochromata-de/de.monochromata.ml/).

## License

LGPL