package de.monochromata.plot;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.function.Consumer;

import javax.swing.JFrame;

/**
 * A trait for window-related behaviour.
 */
public interface Windowing {

	default void showWindowAndWaitUntilItIsClosed(final String windowTitle, final int width, final int height,
			final Consumer<JFrame> windowConfigurator) {
		final JFrame frame = new JFrame(windowTitle);
		frame.setSize(width, height);
		windowConfigurator.accept(frame);
		frame.setVisible(true);
		waitUntilTheWindowIsClosing(frame);
	}

	default void waitUntilTheWindowIsClosing(final JFrame frame) {
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				synchronized (frame) {
					frame.notifyAll();
				}
			}

		});
		synchronized (frame) {
			try {
				frame.wait();
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
