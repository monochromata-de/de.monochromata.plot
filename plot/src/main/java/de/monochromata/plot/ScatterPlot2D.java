package de.monochromata.plot;

import java.awt.Color;
import java.util.function.Supplier;

import javax.swing.JFrame;

import org.math.plot.Plot2DPanel;
import org.math.plot.Plot3DPanel;

/**
 * A Builder for a 2D scatter plot.
 */
public class ScatterPlot2D implements Windowing {

	private final Plot2DPanel plotPanel;

	public ScatterPlot2D() {
		plotPanel = new Plot2DPanel();
	}

	/**
	 * @param orientation
	 *            NORTH, EAST, SOUTH, WEST or INVISIBLE
	 * @return
	 */
	public ScatterPlot2D setLegendOrientation(final String orientation) {
		plotPanel.setLegendOrientation(orientation);
		return this;
	}

	public ScatterPlot2D addScatterPlot(final String legendEntry, final Color color, final Supplier<double[]> xSupplier,
			final Supplier<double[]> ySupplier) {
		return addScatterPlot(legendEntry, color, xSupplier.get(), ySupplier.get());
	}

	public ScatterPlot2D addScatterPlot(final String legendEntry, final Color color, final double[] x, final double[] y) {
		plotPanel.addScatterPlot(legendEntry, color, x, y);
		return this;
	}

	public Plot2DPanel getPanel() {
		return plotPanel;
	}

	public void showPlotAndWaitUntilJFrameIsClosed(final String windowTitle) {
		showWindowAndWaitUntilItIsClosed(windowTitle, 800, 600, frame -> {
			frame.setContentPane(plotPanel);
		});
	}
}
