package de.monochromata.plot;

import java.awt.Color;
import java.util.function.Supplier;

import javax.swing.JFrame;

import org.math.plot.Plot3DPanel;

/**
 * A Builder for a 3D scatter plot.
 */
public class ScatterPlot3D implements Windowing {

	private final Plot3DPanel plotPanel;

	public ScatterPlot3D() {
		plotPanel = new Plot3DPanel();
	}

	/**
	 * @param orientation
	 *            NORTH, EAST, SOUTH, WEST or INVISIBLE
	 * @return
	 */
	public ScatterPlot3D setLegendOrientation(final String orientation) {
		plotPanel.setLegendOrientation(orientation);
		return this;
	}

	public ScatterPlot3D addScatterPlot(final String legendEntry, final Color color, final Supplier<double[]> xSupplier,
			final Supplier<double[]> ySupplier, final Supplier<double[]> zSupplier) {
		return addScatterPlot(legendEntry, color, xSupplier.get(), ySupplier.get(), zSupplier.get());
	}

	public ScatterPlot3D addScatterPlot(final String legendEntry, final Color color, final double[] x, final double[] y,
			final double[] z) {
		plotPanel.addScatterPlot(legendEntry, color, x, y, z);
		return this;
	}

	public Plot3DPanel getPanel() {
		return plotPanel;
	}

	public void showPlotAndWaitUntilJFrameIsClosed(final String windowTitle) {
		showWindowAndWaitUntilItIsClosed(windowTitle, 800, 600, frame -> {
			frame.setContentPane(plotPanel);
		});
	}
}
