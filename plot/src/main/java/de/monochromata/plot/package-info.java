/**
 * Utility classes plotting data for visual inspection.
 */
package de.monochromata.plot;