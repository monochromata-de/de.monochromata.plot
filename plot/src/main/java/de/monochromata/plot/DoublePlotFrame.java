package de.monochromata.plot;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.math.plot.PlotPanel;

/**
 * Shows a JFrame with two plots.
 * 
 * @deprecated Use {@link MultiColumnPlotFrame} instead.
 */
@Deprecated
public class DoublePlotFrame implements Windowing {

	private final String leftTitle;
	private final PlotPanel leftPanel;
	private final String rightTitle;
	private final PlotPanel rightPanel;

	public DoublePlotFrame(final String leftTitle, final PlotPanel leftPanel, final String rightTitle,
			final PlotPanel rightPanel) {
		this.leftTitle = leftTitle;
		this.leftPanel = leftPanel;
		this.rightTitle = rightTitle;
		this.rightPanel = rightPanel;
	}

	public void showPlotAndWaitUntilJFrameIsClosed(final String windowTitle) {
		showWindowAndWaitUntilItIsClosed(windowTitle, 1600, 600, frame -> {
			final JPanel contentPane = new JPanel();
			contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
			contentPane.add(createDiagram(leftTitle, leftPanel));
			contentPane.add(createDiagram(rightTitle, rightPanel));
			frame.setContentPane(contentPane);
		});
	}

	protected JPanel createDiagram(final String title, final PlotPanel plotPanel) {
		final JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(new JLabel(title));
		panel.add(plotPanel);
		return panel;
	}

}
