package de.monochromata.plot;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.math.plot.PlotPanel;

/**
 * Permits a variable number of plots to be displayed in a variable number of columns.
 */
public class MultiColumnPlotFrame implements Windowing {

	private final List<Column> columns;

	public MultiColumnPlotFrame(final List<Column> columns) {
		this.columns = columns;
	}

	public void showPlotAndWaitUntilJFrameIsClosed(final String windowTitle) {
		showWindowAndWaitUntilItIsClosed(windowTitle, 1600, 600, frame -> {
			final JPanel contentPane = new JPanel();
			contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
			columns.stream().forEach(column -> addColumnPanel(contentPane, column));
			frame.setContentPane(contentPane);
		});
	}
	
	protected void addColumnPanel(final JPanel contentPane, final Column column) {
		final JPanel columnPanel = createColumnPanel(column);
		contentPane.add(columnPanel);
	}
	
	protected JPanel createColumnPanel(final Column column) {
		return createColumnPanel(column.getTitle(), column.getPlots());
	}
	
	protected JPanel createColumnPanel(final String title, final List<LabeledPlotPanel> childPanels) {
		final JPanel columnPanel = new JPanel();
		columnPanel.setLayout(new BoxLayout(columnPanel, BoxLayout.Y_AXIS));
		columnPanel.add(new JLabel(title));
		childPanels.stream()
			.map(this::createDiagramPanel)
			.forEach(columnPanel::add);
		return columnPanel;
	}

	protected JPanel createDiagramPanel(final LabeledPlotPanel labeledPanel) {
		final JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(new JLabel(labeledPanel.getLabel()));
		panel.add(labeledPanel.getPlotPanel());
		return panel;
	}
	
	public static class Column {
		
		private final String title;
		private final List<LabeledPlotPanel> plots;
		
		public Column(String title, List<LabeledPlotPanel> plots) {
			this.title = title;
			this.plots = plots;
		}

		public String getTitle() {
			return title;
		}

		public List<LabeledPlotPanel> getPlots() {
			return plots;
		}
		
	}
	
	public static class LabeledPlotPanel {
		
		private final String label;
		private final PlotPanel plotPanel;
		
		public LabeledPlotPanel(final String label, final PlotPanel plotPanel) {
			this.label = label;
			this.plotPanel = plotPanel;
		}

		public String getLabel() {
			return label;
		}

		public PlotPanel getPlotPanel() {
			return plotPanel;
		}
		
	}
	
}
