# Release 1.0.55

* Update dependency org.codehaus.mojo:versions-maven-plugin to v2.9.0

# Release 1.0.54

* Update dependency org.reficio:p2-maven-plugin to v2

# Release 1.0.53

* Update dependency org.assertj:assertj-core to v3.22.0

# Release 1.0.52

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.10.0

# Release 1.0.51

* Update dependency org.assertj:assertj-core to v3.21.0

# Release 1.0.50

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.1

# Release 1.0.49

* Update dependency org.assertj:assertj-core to v3.20.2

# Release 1.0.48

* Update dependency org.assertj:assertj-core to v3.20.1

# Release 1.0.47

* Update dependency org.reficio:p2-maven-plugin to v1.7.0

# Release 1.0.46

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.0

# Release 1.0.45

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.2

# Release 1.0.44

* Update dependency org.reficio:p2-maven-plugin to v1.6.0

# Release 1.0.43

* Update dependency junit:junit to v4.13.2

# Release 1.0.42

* Update dependency org.assertj:assertj-core to v3.19.0

# Release 1.0.41

* Update dependency org.assertj:assertj-core to v3.18.1

# Release 1.0.40

* Update dependency junit:junit to v4.13.1

# Release 1.0.39

* Merge remote-tracking branch 'origin/master' into renovate/org.apache.maven.plugins-maven-project-info-reports-plugin-3.x

# Release 1.0.38

* Merge remote-tracking branch 'origin/master' into renovate/org.assertj-assertj-core-3.x

# Release 1.0.37

* Merge remote-tracking branch 'origin/master' into renovate/org.codehaus.mojo-versions-maven-plugin-2.x
* Merge remote-tracking branch 'origin/master' into renovate/org.codehaus.mojo-versions-maven-plugin-2.x

# Release 1.0.36

* Correct use of baseUri
* Correct reference to parent m2 repo

# Release 1.0.35

* Correct name of aggregator POM

# Release 1.0.34

* Also release parent POM so it is available downstream

# Release 1.0.33

* Publish m2 and p2 repos to GitLab Pages

# Release 1.0.32

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.1

# Release 1.0.31

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.0

# Release 1.0.30

* Update dependency org.assertj:assertj-core to v3.16.1

# Release 1.0.29

* Update dependency org.assertj:assertj-core to v3.16.0

# Release 1.0.28

* Update dependency org.apache.maven.wagon:wagon-ssh to v3.4.0

# Release 1.0.27

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.2.0

# Release 1.0.26

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.0

# Release 1.0.25

* Update dependency org.assertj:assertj-core to v3.15.0

# Release 1.0.24

* No changes

# Release 1.0.23

* No changes

# Release 1.0.22

* No changes

# Release 1.0.21

* No changes

# Release 1.0.20

* No changes

# Release 1.0.19

* No changes

# Release 1.0.18

* No changes

# Release 1.0.17

* No changes

